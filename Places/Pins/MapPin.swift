//
//  MapPin.swift
//
//
//  Created by Mohamed Badreldin on 29/01/2018.
//  Copyright © 2018 Mohamed Badreldin. All rights reserved.
//

import MapKit
//import Contacts

class MapPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var placeId:String?
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D , placeId : String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = locationName
        self.placeId = placeId
    }
}
