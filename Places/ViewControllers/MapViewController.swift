//
//  MapViewController.swift
//  Places
//
//  Created by Mohamed Badreldin on 29/05/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    var mapView: MKMapView?
    
    var currentLocation: CLLocation!{
        didSet {
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 200, 200)
            self.mapView?.setRegion(viewRegion, animated: false)
            
        }
    }
    
    var arrData: NSMutableArray!{
        didSet {
            print(arrData)
            for mapItem in arrData {
                
                
                let lat = (((mapItem as! NSDictionary).value(forKeyPath: "geometry") as! NSDictionary).value(forKeyPath: "location")as! NSDictionary).value(forKeyPath: "lat") as! NSNumber
                let lng = (((mapItem as! NSDictionary).value(forKeyPath: "geometry") as! NSDictionary).value(forKeyPath: "location")as! NSDictionary).value(forKeyPath: "lng") as! NSNumber
                let title = (mapItem as! NSDictionary).value(forKeyPath: "name") as! String
                let placeId = (mapItem as! NSDictionary).value(forKeyPath: "place_id") as! String
                
                let tempCoordinate = CLLocation(latitude: lat.doubleValue, longitude: lng.doubleValue)
                
                let distanceInMeters = String(format: "%.2f meters", currentLocation.distance(from: tempCoordinate))
                
                let mappin = MapPin(title: title,
                                    locationName: distanceInMeters,
                                    coordinate: CLLocationCoordinate2D(latitude: lat.doubleValue , longitude: lng.doubleValue),
                                    placeId:placeId )
                
                DispatchQueue.main.async {
                    self.mapView?.addAnnotation(mappin)
                }
            }
            DispatchQueue.main.async {
                
                var zoomRect: MKMapRect = MKMapRectNull
                for annotation in (self.mapView?.annotations)! {
                    guard  annotation is MapPin else {
                        print("User Pin")
                        return
                    }
                    let artwork = annotation as! MapPin
                    
                    let annotationPoint: MKMapPoint = MKMapPointForCoordinate((artwork.coordinate))
                    let pointRect: MKMapRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
                    if MKMapRectIsNull(zoomRect) {
                        zoomRect = pointRect
                    } else {
                        zoomRect = MKMapRectUnion(zoomRect, pointRect)
                    }
                }
                self.mapView?.setVisibleMapRect(zoomRect, animated: true)
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView = MKMapView(frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height))
        
        self.view.addSubview(self.mapView!)
        
        self.mapView?.showsUserLocation = true
        self.mapView?.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MapPin else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            
//            //right
//            let btnDetailButton = UIButton(type: .detailDisclosure)
//            btnDetailButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
//            btnDetailButton.contentVerticalAlignment = .center
//            btnDetailButton.contentHorizontalAlignment = .center
//            view.rightCalloutAccessoryView = btnDetailButton
//
            //left
            let btnDirectionButton = UIButton(type: .detailDisclosure)
            btnDirectionButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
            btnDirectionButton.contentVerticalAlignment = .center
            btnDirectionButton.contentHorizontalAlignment = .center
            
            view.leftCalloutAccessoryView = btnDirectionButton
        }
        return view
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! MapPin

        let url = String(format: "https://www.google.com/maps/search/?api=1&query=Google&query_place_id=%@",location.placeId!)
        
        
        if let link = URL(string: url) {
            UIApplication.shared.open(link)
        }
    }
}
