//
//  TabsViewController.swift
//  Places
//
//  Created by Mohamed Badreldin on 29/05/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import UIKit
import CoreLocation


class TabsViewController: UITabBarController, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    let MapVC = MapViewController()
    let TableVC = TableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        
        
        MapVC.title = "Map"
        MapVC.view.backgroundColor = UIColor.orange
        
        
        TableVC.title = "Table"
        TableVC.view.backgroundColor = UIColor.blue
        
        MapVC.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 0)
        TableVC.tabBarItem = UITabBarItem(tabBarSystemItem: .featured, tag: 1)
        let controllers = [TableVC, MapVC]
        
        self.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        
        
  
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            currentLocation = nil
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = locations[locations.count - 1]
        TableVC.currentLocation = locations[locations.count - 1]
        MapVC.currentLocation = locations[locations.count - 1]
        locationManager.stopUpdatingLocation()

        PlacesWebTask.googleCall(searchLocation: currentLocation, onResult: { (data , error) in
            
            
            self.TableVC.arrData = data
            self.MapVC.arrData = data
            
            
            
        })
    }
    
    
}
