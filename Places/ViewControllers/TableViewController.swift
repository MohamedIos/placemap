//
//  ViewController.swift
//  Places
//
//  Created by Mohamed Badreldin on 29/05/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import UIKit
import CoreLocation

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var myTableView: UITableView!
    var currentLocation: CLLocation!
    var arrData: NSMutableArray!{
        didSet {
            print(arrData)
            DispatchQueue.main.async {
               self.myTableView.reloadData()
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
        arrData = NSMutableArray()

        arrData.removeAllObjects()

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:TableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let   mapItem = arrData[indexPath.row]
        let placeId = (mapItem as! NSDictionary).value(forKeyPath: "place_id") as! String
//
        if let link = URL(string: "https://www.google.com/maps/search/?api=1&query=Google&query_place_id=\(placeId)") {
            UIApplication.shared.open(link)
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        //        cellStyle = UITableViewCellStyleValue2;
        
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyCell")
        
        
        let mapItem = arrData[indexPath.row]
        
        
        let lat = (((mapItem as! NSDictionary).value(forKeyPath: "geometry") as! NSDictionary).value(forKeyPath: "location")as! NSDictionary).value(forKeyPath: "lat") as! NSNumber
        let lng = (((mapItem as! NSDictionary).value(forKeyPath: "geometry") as! NSDictionary).value(forKeyPath: "location")as! NSDictionary).value(forKeyPath: "lng") as! NSNumber
        let title = (mapItem as! NSDictionary).value(forKeyPath: "name") as! String
        //        let placeId = (mapItem as! NSDictionary).value(forKeyPath: "place_id") as! String
        
        let tempCoordinate = CLLocation(latitude: lat.doubleValue, longitude: lng.doubleValue)
        
        let distanceInMeters = String(format: "%.2f meters", currentLocation.distance(from: tempCoordinate))
        
        cell.textLabel!.text = title
        cell.detailTextLabel!.text = distanceInMeters
        cell.accessoryType = .disclosureIndicator

        
        return cell
    }
}

