//
//  PlacesWebTask.swift
//  Places
//
//  Created by Mohamed Badreldin on 29/05/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import UIKit
import CoreLocation

class PlacesWebTask: NSObject {
    static let base = "https://maps.googleapis.com/maps/api"
    static let googlePlace = "\(base)/place/nearbysearch/json?"
    static let googleKey = "AIzaSyCuvE-90qXrTU7udJCYsy5NH-xQN4mV-VI"
    

    static func googleCall(searchLocation location : CLLocation, onResult:  @escaping (NSMutableArray, NSError?) -> Void) {
          
        let url = URL(string:  googlePlace.appending("location=\(location.coordinate.latitude),\(location.coordinate.longitude)&type=bar&rankby=distance&key=\(googleKey)"))
    
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard data != nil else {
                print("Data is empty")
                return
            }
            var json = NSDictionary()
            do {
                json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                
            } catch {
                print(error)
                return
            }
            
            if let _ = json["results"] {
                let data = json["results"] as! NSMutableArray
                onResult(data, nil)
            }
            
//            onResult([], error! as NSError)
            
        }
        
        task.resume()
    }
   

}
